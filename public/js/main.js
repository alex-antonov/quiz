$(document).ready(function () {
    let quizContainer = $('.quiz');
    let question = 1;
    var score = 0;

    function questionNumber() {
        $('.quiz__title').text('0' + question)
    }

    function questionTitle() {
        $('.quiz__sub-title').text(quizMap[question].text)
    }

    function validateRadio() {
        let answer = $('.answers__radio-elem:checked');
        let error = $('.answers__error');
        if (answer.length > 0) {
            error.text('');
            return true;
        }
        error.text('Выберите один вариант ответа');
        return false;
    }

    function validateName() {
        let name = $('#name');
        let error = name.next('div.form__error');
        if (name.val() === '') {
            error.text('Обязятельное поле').show();
            return false;
        }
        error.hide();
        return name.val();
    }

    function validateEmail() {
        let email = $('#email');
        let error = email.next('div.form__error');
        if (email.val() === '') {
            error.text('Обязятельное поле').show();
            return false
        }
        const regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(email.val())) {
            error.text('Некорректный email').show();
            return false
        }
        error.hide();
        return email.val();
    }

    function resume() {
        if (score < 15) {
            return result.jun;
        }
        if (score > 15 && score < 20) {
            return result.mid;
        }
        return result.sen;
    }

    function answerTpl() {
        let answers = '';
        for (let i = 0; i < 4; i++) {
            answers +=
                '<li class="answers-item answers__control">' +
                '                            <label class="answers__label">\n' +
                '                                <input type="radio" name="answer" class="answers__radio-elem" data-score="' + quizMap[question].answers[i].point + '">\n' +
                '                                <div class="answers__radio-fake"></div>\n' +
                '                                <span class="answers__text">' + quizMap[question].answers[i].text + '</span>\n' +
                '                            </label>\n' +
                '                        </li>\n'
        }
        return answers;
    }

    function activeNav() {
        $(".quiz__nav").find("li").eq(question - 1).addClass('active');
    }

    $('#start-btn').on('click', function (e) {
        e.preventDefault();
        let questionImg = quizMap[question].img;
        quizContainer.empty();
        questionNumber();
        quizContainer.append('<div class="quiz__content quiz--question">\n' +
            '                    <h2 class="quiz__title">\n' +
            '                        01\n' +
            '                    </h2>\n' +
            '                    <h3 class="quiz__sub-title">\n' +
            '                        Вы решили заняться бизнесом. Для начала определитесь, какой вид бизнеса Вам более\n' +
            '                        интересен.</h3>\n' +
            '                    <ul class="quiz__nav">\n' +
            '                        <li class="quiz__nav-item active"></li>\n' +
            '                        <li class="quiz__nav-item"></li>\n' +
            '                        <li class="quiz__nav-item"></li>\n' +
            '                        <li class="quiz__nav-item"></li>\n' +
            '                        <li class="quiz__nav-item"></li>\n' +
            '                        <li class="quiz__nav-item"></li>\n' +
            '                        <li class="quiz__nav-item"></li>\n' +
            '                        <li class="quiz__nav-item"></li>\n' +
            '                        <li class="quiz__nav-item"></li>\n' +
            '                    </ul>\n' +
            '                    <ul class="answers__list">\n' +
            answerTpl() +
            '                    </ul>\n' +
            '                   <div class="answers__error"></div> ' +
            '                    <a id="next" href="#" class="quiz_button next">Следующий вопрос</a>' +
            '                </div>\n' +
            '                                    <img src=' + questionImg + ' alt="" class="quiz__img">\n'
        )
        question++;
    });

    $(document).on('click', '.next', function (e) {
        e.preventDefault();
        if (validateRadio() === false) {
            return
        }
        score += parseInt($('.answers__radio-elem:checked').attr('data-score'));
        activeNav();
        questionNumber();
        questionTitle();
        $('.answers__list').empty().append(answerTpl());
        $('.quiz__img').attr("src", quizMap[question].img);
        if (question === 9) {
            $('.quiz_button').removeClass('next').addClass('last')
        }
        question++;
    });

    $(document).on('click', '.last', function (e) {
        e.preventDefault();
        quizContainer.empty();
        quizContainer.append('        <div class="quiz__content quiz--result">\n' +
            '                    <h2 class="quiz__sub-title">\n' +
            '                        Тест пройден\n' +
            '                    </h2>\n' +
            '                    <h3 class="quiz__title">Оставьте ваши контактные данные и получите Ваш результат с 2 полезными статьями</h3>\n' +
            '                    <form action="form.php" class="form">' +
            '                        <div class="form__item">' +
            '                            <input id="name" type="text" placeholder="ФИО" class="form__item-input">' +
            '                            <div class="form__error"></div>' +
            '                        </div>' +
            '                        <div class="form__item">' +
            '                            <input id="email" type="text" placeholder="E-mail" class="form__item-input">' +
            '                            <div class="form__error"></div>' +
            '                        </div>' +
            '                    </form>' +
            '                    <a id="submit" href="#" class="quiz_button submit">Получить ответ</a>' +
            '                </div>' +
            '                <img src="img/contacts.png" alt="" class="quiz__img">'
        );
    });

    $(document).on('click', '.submit', function (e) {
        e.preventDefault();
        let validName = validateName();
        let validEmail = validateEmail();
        if (validName && validEmail) {
            sendMail(validName, validEmail);
            quizContainer.empty();
            let res = resume();
            quizContainer.append('        <div class="quiz__content quiz--result">' +
                '                    <h3 class="quiz__sub-title">' +
                '                        Спасибо, что прошли текст на выявление типа предпринимателя! ' +
                '                    </h3>' +
                '                    <h2 class="quiz__title">Ваш тип препринимателя:</h2>' +
                '                     <p>' + res.text + '</p>' +
                '                    <a href="https://altair360.com/blog/zachem-nuzhno-vnedryat-tehnologii-vr-ar-v-obrazovatelnyj-protsess" target="_blank" class="quiz_button result">Полезная статья №1</a>' +
                '                    <a href="https://altair360.com/blog/altair-digital-vremya-pokoryat-novye-vershiny" target="_blank" class="quiz_button result">Полезная статья №2</a>' +
                '                </div>' +
                '                <img src="' + res.img + '" alt="" class="quiz__img">'
            );
        }
    });

    $(document).on('click', '#name', function (e) {
        $('#name').next('div.form__error').hide();
    });

    $(document).on('click', '#email', function (e) {
        $('#email').next('div.form__error').hide();
    });

    function sendMail(name, email) {
        $.ajax({
            type: "POST",
            url: 'form.php',
            data: {
                name: name,
                email: email
            },
            success: function (data) {
                console.log('success')
            },
            error: function (data) {
                console.log('error')
            }
        });

    }


    const quizMap = {
        1: {
            text: 'Вы решили заняться бизнесом. Для начала определитесь, какой вид бизнеса Вам более интересен.',
            img: '/img/quiz-1.png',
            answers: [
                {
                    id: 1,
                    text: 'Что-то, в чем я хорошо разбираюсь и справлюсь со всем сам',
                    point: 1
                },
                {
                    id: 2,
                    text: 'Актуальный вид бизнеса, который будет востребован всегда',
                    point: 3
                },
                {
                    id: 3,
                    text: 'Сделаю свой собственный продукт',
                    point: 1
                },
                {
                    id: 4,
                    text: 'Производство дешевого продукта, массовое потребление',
                    point: 2
                }
            ]
        },
        2: {
            text: 'Вам необходима команда, как будете её собирать?',
            img: '/img/quiz-2.png',
            answers: [
                {
                    id: 5,
                    text: 'Приглашу родственников и друзей',
                    point: 2
                },
                {
                    id: 6,
                    text: 'Размещу вакансию на сайтах поиска работы',
                    point: 3
                },
                {
                    id: 7,
                    text: 'Размещу вакансию в соцсетях',
                    point: 3
                },
                {
                    id: 8,
                    text: 'Расклею листовки по подъездам',
                    point: 1
                }
            ]
        },
        3: {
            text: 'Будете ли Вы рекламировать свой бизнес?',
            img: '/img/quiz-3.png',
            answers: [
                {
                    id: 9,
                    text: 'Нет, не буду, буду сам искать клиентов, холодным обзвоном',
                    point: 2
                },
                {
                    id: 10,
                    text: 'Сделаю сайт и заведу страницы в социальных сетях',
                    point: 3
                },
                {
                    id: 11,
                    text: 'Подам объявление в газету',
                    point: 1
                },
                {
                    id: 12,
                    text: 'Не буду экономить на рекламе, задействую все ресурсы',
                    point: 3
                }
            ]
        },
        4: {
            text: 'Ваш бизнес набирает популярность. Его начинают обсуждать в соцсетях, высказывать недовольство. Ваша реакция',
            img: '/img/quiz-4.png',
            answers: [
                {
                    id: 13,
                    text: 'Ничего не буду делать, черный пиар - это тоже пиар',
                    point: 3
                },
                {
                    id: 14,
                    text: 'Отвечу на недовольство, буду оспаривать точку зрения, в крайнем случае подам в суд за клевету',
                    point: 1
                },
                {
                    id: 15,
                    text: 'Заведу “левую” страничку, не соглашусь с недовольством',
                    point: 2
                },
                {
                    id: 16,
                    text: 'Лично извинюсь, предложу скидку или подарок от компании',
                    point: 1
                }
            ]
        },
        5: {
            text: 'Ваши менеджеры по продажам теряют клиентов, что Вы будете делать',
            img: '/img/quiz-5.png',
            answers: [
                {
                    id: 17,
                    text: 'Уволю всех и найму новых',
                    point: 1
                },
                {
                    id: 18,
                    text: 'Найму тренера по продажам, проведу обучение и сам узнаю новое',
                    point: 3
                },
                {
                    id: 19,
                    text: 'Обучу сотрудников, сам. Это мой бизнес, я лучше знаю как продавать',
                    point: 1
                },
                {
                    id: 20,
                    text: 'Найму опытного руководителя отдела продаж',
                    point: 2
                }
            ]
        },
        6: {
            text: 'Вы успешно закончили год перевыполнив план, куда будете инвестировать сверх выручку?',
            img: '/img/quiz-6.png',
            answers: [
                {
                    id: 21,
                    text: 'Выдам сверх премию сотрудникам',
                    point: 2
                },
                {
                    id: 22,
                    text: 'Поеду в отпуск, я это заслужил',
                    point: 1
                },
                {
                    id: 23,
                    text: 'Направлю инвестиции в рекламу',
                    point: 3
                },
                {
                    id: 24,
                    text: 'Найму маркетолога',
                    point: 3
                }
            ]
        },
        7: {
            text: 'Вы решили устроить летний корпоратив, где его организуете',
            img: '/img/quiz-7.png',
            answers: [
                {
                    id: 25,
                    text: 'Оплачу ресторан, сотрудники заслужили отдых',
                    point: 2
                },
                {
                    id: 26,
                    text: 'Устроим выезд на природу, арендую базу отдыха, сделаем тимбилд с тренером',
                    point: 3
                },
                {
                    id: 27,
                    text: 'Приглашу всех к себе на дачу, устрою семейный отдых, мы семья',
                    point: 2
                },
                {
                    id: 28,
                    text: 'Ничего не нужно, лучше просто выходной им дать',
                    point: 1
                }
            ]
        },
        8: {
            text: 'Местные СМИ решили написать о Вас. Что Вы будете рассказывать в интервью?',
            img: '/img/quiz-8.png',
            answers: [
                {
                    id: 29,
                    text: 'Я сделал все сам, ничего не боялся и поэтому у меня все вышло',
                    point: 1
                },
                {
                    id: 30,
                    text: 'Это командная работа, успех принадлежит всем',
                    point: 3
                },
                {
                    id: 31,
                    text: 'Спасибо моим родителям, что верили в меня',
                    point: 2
                },
                {
                    id: 32,
                    text: 'Это легкий бизнес, с ним любой справится',
                    point: 1
                }
            ]
        },
        9: {
            text: 'От чего зависит успех в бизнесе?',
            img: '/img/quiz-9.png',
            answers: [
                {
                    id: 33,
                    text: 'Только от меня, я владелец бизнеса',
                    point: 1
                },
                {
                    id: 34,
                    text: 'От всей команды, каждый вносит свой вклад',
                    point: 3
                },
                {
                    id: 35,
                    text: 'От экономического положения в стране',
                    point: 2
                },
                {
                    id: 36,
                    text: 'От вида бизнеса, его актуальности на рынке',
                    point: 3
                }
            ]
        }
    };
    const result = {
        jun: {
            text: 'Вы сами любите заниматься всеми делами в компании, т.к. никто кроме Вас не знает как лучше выстроить все этапы производства и добиться конечного результата. Даже простую задачу - Вы не делегируете подчиненному, т.к привыкли все держать под контролем. Очевидно, Вам подойдет бизнес, где Вы будете принимать активное участие и контролировать все процессы: начиная с поиска целевой аудитории и заканчивая закрытием сделки.',
            img: '/img/results-jun.png'
        },
        mid: {
            text: 'В Вашей команде успех достигается равноправным участием каждого сотрудника. Все вместе Вы завоевываете все новые и новые вершины. Если один не успевает справиться с задачей, коллеги всегда поддержат и помогут выполнить все в срок. Да и Вы не боитесь заниматься самыми простыми делами. В бизнесе Вам подойдет направление "командной игры", где каждый участник, наравне с Вами будет вносить свой вклад.',
            img: '/img/results-mid.png'
        },
        sen: {
            text: 'Вы поклонник сверхдоверительных отношений. Корпоративный дух в вашей компании — залог успеха. И для таких, как Вы, коллеги - это семья. Вы не только успешный руководитель, умеющий правильно строить работу команды и выстраивать планы развития предприятия, но и надежный друг для всех сотрудников, который никогда не оставит в беде и всегда поможет в сложной ситуации.',
            img: '/img/results-sen.png'
        }
    };
});
