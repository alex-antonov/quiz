<?php

use PHPMailer\PHPMailer\PHPMailer;

require __DIR__ . '/../vendor/autoload.php';
$config = require __DIR__ . '/../config/smtp.php';

if (isset($_POST['name']) && isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    $email = $_POST['email'];
    $name = strip_tags($_POST['name']);
    $mail = new PHPMailer();
    try {
        $mail->isSMTP();
        // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';                      // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;
        $mail->Username = $config['smtp']['username'];                 // SMTP username
        $mail->Password = $config['smtp']['password'];
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = '465';
        $mail->CharSet = 'UTF-8';

        $mail->setFrom('antonov@altair360.com', 'Quiz Lid');
//    $mail->addAddress('dm@altair.fm ', 'Дмитрий Федорович');
        $mail->addAddress('antonovsasha22@gmail.com', 'Александр Антонов');

        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Лид с квиза';
        $mail->Body = "Лид <b>{$name}</b>. Почта <b>{$email}</b>";
        $mail->AltBody = "Лид {$name}. Почта {$email}";
        if ($mail->send()) {
            echo 'send';
        } else {
            echo $mail->ErrorInfo;
        }
    } catch (\Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}
